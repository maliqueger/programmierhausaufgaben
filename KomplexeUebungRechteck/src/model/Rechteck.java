package model;
import java.util.Iterator;
import java.lang.Math;
import model.Punkt;
public class Rechteck {
  

 // private int x;
 // private int y;
  private Punkt p;
  private int breite;
  private int hoehe;
  private int maxHoehe = 1000;
  private int maxBreite = 1200;
  public static int breiteZuf;
  public static int hoeheZuf;
  
  public Rechteck() {
	 this.p = new Punkt(0,0);
    // this.x = 0;
    // this.y = 0;
    this.breite = 0;
    this.hoehe = 0;
  }
  public Rechteck(int x, int y, int breite, int hoehe) {
	  this.p = new Punkt(0,0);
	    // this.x = x;
	    // this.y = y;
	    this.breite = 0;
	    this.hoehe = 0;
	  }
  public static Rechteck generiereZufallsRechteck(int maxBreite, int maxHoehe) {
	int hoeheZuf = 0+ (int) (Math.random() * ( maxHoehe - 0)+1);
	int breiteZuf = 0+ (int) (Math.random() * ( maxBreite - 0)+1);
	return generiereZufallsRechteck(hoeheZuf, breiteZuf);	
	}
  

	
  

public int getX() {
	return this.p.getX();
	
	}


public void setX(int x) {
	this.p.getX();
}

public int getY() {
	return this.p.getY();
}

public void setY(int y) {
	this.p.getY();
}

public int getBreite() {
	if (breite<0) {
		breite = breite + (-2)*breite; 
	}
	return breite;
	
}

public void setBreite(int breite) {
	this.breite = breite;
}

public int getHoehe() {
	if (hoehe<0) {
		hoehe = hoehe + (-2)*hoehe; 
	}
	
	return hoehe;
}

public void setHoehe(int hoehe) {
	this.hoehe = hoehe;}

public boolean enthaelt(int x, int y) {
	Punkt p = new Punkt(x,y);
	return enthaelt(p);
}
public boolean enthaelt(Punkt p) {
	int xp = p.getX();
	int xy = p.getY();
	int x = this.getX();
	int y = this.getY();
	int hoehe = this.getHoehe();
	int breite = this.getBreite();
	
	if((xp<=xp+breite)&&(xy >= x)&&(xy <= y+hoehe)&&(xy>=y)) {
		return true;
	} else {
	return false;
}
}
public boolean enthaelt(Rechteck rechteck0) {
	int hoehe1 = rechteck0.getHoehe();
	int breite1 = rechteck0.getBreite();
	int hoehe = this.getHoehe();
	int breite = this.getBreite();
	
if ((breite1 >= breite) && (hoehe1 >= hoehe)) {
return true;
} else {
return false;
}
}



@Override
public String toString() {
    return "Rechteck[x="+getX()+", y="+getY()+", breite="+getBreite()+", hohe="+getHoehe()+"]";
//4.: public String toString() {	return ""+getX()+","+getY()+","+getBreite()+","+getHoehe();    //del       
                 
  
} 

}