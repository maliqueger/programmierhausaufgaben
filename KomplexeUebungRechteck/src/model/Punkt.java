package model;

public class Punkt {
	 
	  private int x;
	  private int y;
	  
	  public Punkt() {  //Konstruktoren
	    this.x = 0;
	    this.y = 0;
	  }
	  public Punkt(int x, int y) {
		    this.x = x;
		    this.y = y;
		  }
		
		

	public int getX() {  //getter setter
		return x;
		
		}


public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	public boolean equals(Punkt p) {
		return true; }

	@Override //sAmqUB3aWvR44gxJQeK9
	public String toString() {
	    return "Punkt[x="+getX()+", y="+getY()+"]"; 
	
	                 
	  
	} 
	}
//sAmqUB3aWvR44gxJQeK9